# Preface

This is a personal project to try and not only educate myself but to spread the knowledge to others about Bitcoin and how it works for those who are eager to learn more about it but find it very confusing. To give some background, I have always been fascinated by mathematics and have always loved coding from the moment I discovered it in high school. I have studied Aero-Mechanical Engineering in Strathclyde and only recently shifted to become a Software Developer in 2016. I first heard about Bitcoin early 2017 but was reluctant to look into it for some reason. I among many others just thought it was some sort of "magic internet money", and although I didn't think it was a scam, I always thought it would be too difficult to even try and understand how it worked so I just ignored it. It wasn't until early 2018 that I finally decided to give it a chance and do proper research on the matter that I realised just how wonderful Bitcoin can be for the world we live in. I've been learning about Bitcoin for over a year now and I still have much to learn but I hope that I can share some of the knowledge with anyone else who is also at a position like I was in the past and is lost for a starting point. This is aimed at an introductory level and will mostly be covering about Bitcoin on a high level rather than a technical level on how it works therefore no prior coding knowledge or mathematics is required, though it will help.

## README

![build status](https://gitlab.com/waigetlaw/introduction-to-bitcoin/badges/master/build.svg)

The website version of this eBook can be found here: [Introduction To Bitcoin](https://waigetlaw.gitlab.io/introduction-to-bitcoin/)

This website/eBook was created using a tool called [GitBook](https://www.gitbook.com/) integrated with GitLab. If you are interested in creating your own ebook, this was the template that I used to get started albeit it is in Japanese: [gitbookExample](https://gitlab.com/Ouvill/gitbookexample)

The source code for this website/eBook can be found here: [source on GitLab](https://gitlab.com/waigetlaw/introduction-to-bitcoin)

### Build

To build the ebook you first need to have gitbook installed:

```
npm install -g gitbook-cli
```

Then you can run the commands:

```
npm install
gitbook install
gitbook build . public
```

### Run locally

To run a local version of the ebook:

`gitbook serve`

### Generate eBook files

You can generate an eBook file (ePub, Mobie, PDF) from the source code by downloading the [Calibre application](https://calibre-ebook.com/download) and adding the application folder to your environment variable path. This is necessary as the command `ebook-convert` is required to generate ebooks. Afterwards, make sure you have gitbook installed globally (see above) and run `gitbook install` then one of the following:

#### PDF

In order to generate a PDF version:

`gitbook pdf ./ ./optionalNameOfBook.pdf`

#### ePub

In order to generate an ePub version:

`gitbook epub ./ ./optionalNameOfBook.pdf`

#### Mobi

In order to generate a Mobi version:

`gitbook mobi ./ ./optionalNameOfBook.pdf`

If a name is not given, the output will default to book.pdf

## Donations

This will always be free for everyone but if you like my work and would like to show some support I would really appreciate it.

[Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=596MVU35ATDCS&source=url)
