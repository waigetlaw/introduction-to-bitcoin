# Summary

* [Preface](README.md)

---

* [What Is Bitcoin?](source/intro/00_WhatIsBitcoin.md)
    * [Decentralization](source/intro/01_Decentralization.md)
    * [Transparency](source/intro/02_Transparency.md)
    * [Censorship Resistant](source/intro/03_CensorshipResistant.md)
    * [Unconfiscatable](source/intro/04_Unconfiscatable.md)
    * [Security](source/intro/05_Security.md)
    * [Comparisons](source/intro/06_Comparison.md)
    * [Further Reading](source/intro/07_FurtherReading.md)

---

* [The Blockchain](source/blockchain/00_Blockchain.md)
    * [Addresses](source/blockchain/01_Addresses.md)
    * [Transactions](source/blockchain/02_Transactions.md)
    * [Blocks](source/blockchain/03_Blocks.md)
    * [Mining (Proof of Work)](source/blockchain/04_Mining.md)

---

* [Advanced Topics](source/advanced/00_Advanced.md)
    * [Soft Forks & Hard Forks](source/advanced/SoftForkHardFork.md)
    * [Dandelion](source/advanced/Dandelion.md)
    * [PoW vs PoS](source/advanced/PowVsPos.md)
    * [Attacking The Network](source/advanced/AttackingTheNetwork.md)
    * [Block Time](source/advanced/BlockTime.md)
    * [Block Size](source/advanced/BlockSize.md)
    * [Environmental Impact](source/advanced/EnvironmentalImpact.md)
    * [Merkle Root](source/advanced/MerkleRoot.md)

---

* [References](source/References.md)
* [Appendix](source/Appendix.md)

---

* [Glossary](custom_glossary.md)

---

* [About Me](source/About.md)
