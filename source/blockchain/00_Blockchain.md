# The Blockchain

Now that we know what Bitcoin is, I would like to explain how it works under the hood. Bitcoin is the first of its kind, known as blockchain technology and many other cryptocurrencies out there follow a similar model. The general model consists of mainly four parts:

* Addresses
* Transactions
* Blocks
* Mining (Consensus Model)

We need to understand the functionality of each part of the blockchain before we can understand how they interact and integrate with each other. I will not completely go over every detail but just enough to get across the idea of blockchain technology.