## Mining (Proof of Work)

Proof of work is what Satoshi Nakamoto has given us in his paper for Bitcoin: A Peer-to-Peer Electronic Cash System. Without Proof of work, there is no blockchain and despite what others may have you believe, in the last 10 years there has been no alternative solution that is as secure and elegant as proof of work (shortened to PoW). What proof of work does is provide us with a consensus model - it lets everyone agree on a result without having to trust each other.

### So How Does it Work?

As a miner, your goal is to produce blocks. The steps to doing so is as follows:

1. Choose valid transactions from the mempool (generally one with highest fees as you get the fees as a reward on block completion)
2. Fill in the block header fields, such as merkleRoot of transactions that you have selected
3. Find the hash of your block
4. Find the nonce such that the hash of the hash of the block + nonce will produce a value lower than the target

Hash algorithms are one-way functions - that is, it is impossible to know what the input should be for a desired output. When we have filled in all the other fields for our block, all you can do to figure out the nonce is by trial and error and hope you find a solution before anyone else - it is literally looking for a needle in a haystack but in a mathematical sense.

#### Example:

Block hash = **0123456789abcdef** and target (difficulty) is **70,000**:

---

**Try nonce = 1:**<br>`hash(0123456789abcdef + 1) = 99,999`<br><br>99,999 is higher than the target difficulty of 70,000 so try again:

---

**Try nonce = 2:**<br>`hash(0123456789abcdef + 2) = 123,456`<br><br>Still not equal to or below 70,000 so keep going and eventually:

---

**Try nonce = 5234:**<br>`hash(0123456789abcdef + 5234) = 43,127`<br><br>43,127 is lower than the target 70,000 and therefore **5,234** is a valid nonce for the block.

---

> **_NOTE:_** The '+' sign is used as a sign of concatenation, therefore '0123456789abcdef + 5234' is equal to 0123456789abcdef5234.

Due to this difficult problem, it is impossible for a single party to produce blocks faster than the network. The network will also always choose the **longest chain as the truth** and therefore, unless you can produce blocks at a faster rate than the rest of the network, you cannot control what transactions the blockchain includes or excludes. If one had enough CPU power to mine (known as 51% attack) then one could theoretically control which transactions get through and which ones don't make it onto the blockchain (breaking censorship resistance) as his chain would always be longer than the other 49% of honest miners on the network. However, more is explained about this in the advanced section. The target, also known as the difficulty is adjusted every two weeks based on the network mining performance to regulate the rate in which blocks are created to roughly 2016 blocks every two weeks (one every 10 minutes).

### Consensus

Proof of Work not only provides censorship resistance as it is a way to randomize who the next block producer is but it also provides consensus through this. Back to the double spending problem - if one was to try and send his full balance twice out of his account, proof of work is able to always prevent one of them from going through. There are 3 possible scenarios it does this:

##### 1. Both Transactions Processed by Same Miner

This is a trivial scenario as the miner can see the person has balance X in the account and is trying to send a total greater than X. The miner will choose one of them and discard the other one when producing his block. If he tried to put both transactions in, even if he finds the nonce for the block, the other miners will disregard his block as they can easily verify that the transactions don't add up nicely.

##### 2. One of the Transactions Processed by a Miner

This is what is likely to happen - one of your transactions is picked up by a miner and a valid block is produced and propagated through the network. All other miners validate the block and accept it and will discard the other transaction as it is deemed invalid with the new updated account balance.

##### 3. Both Transactions Processed by Different Miners

In this scenario, it will cause a conflict in the network. It is possible that both transactions are processed by a different miner and both miners find a nonce to create a valid block at roughly the same time. Both valid blocks will be propagated through the network and nodes in the "middle" will not know which transaction to accept. In this case, what happens is that the nodes will take whichever block they received first and continue to search for the next block on top of that one. This means the network will be split and actively searching for the next block on top of two different blocks. At some point, another block will be created and propagated and since this chain is now longer than the other one, all the transactions in the shorter block will be discarded and accept the new longer chain and the network will come back to a consensus. Note, it is theoretically possible that this clash continues forever but the chance of finding two valid blocks at roughly the same time forever is practically impossible - however, most people do take precaution and won't accept a transaction until at least 6 confirmations has occurred to prevent someone from double spending their Bitcoin. A confirmation is any valid blocks mined after and including the block with the transaction in it. Therefore 6 confirmations occurs when the transaction is mined with 5 more blocks on top of the one containing it. There is nothing special about the number 6 apart from being universally agreed that it is a "safe enough" number.

>There is nothing special about the default, often-cited figure of 6 blocks. It was chosen based on the assumption that an attacker is unlikely to amass more than 10% of the hashrate, and that a negligible risk of less than 0.1% is acceptable.

*Taken from https://en.bitcoin.it/wiki/Confirmation*

### Incentives

Since mining uses a lot of electricity to solve for PoW, there needs to be incentives for the miner to mine. The Bitcoin network has a fixed total value of 21 million Bitcoins that will ever exist. They come into existance through mining. When the Bitcoin network first started, miners were awarded with 50 Bitcoin for each successful block produced. This value is halfed roughly every 4 years. As of writing, 16th June 2019, the reward is set at 12.5 bitcoin per block. The reward is a special transaction in the block known as the coinbase transaction which is always the first transaction in a block. The miners also get the fees of all transactions placed in the block so there is incentive for the miner to pack as many transactions as possible that spent the highest fees. Due to Bitcoin reward halving - eventually some time around year 2140, all 21 million Bitcoin will be mined and the reward for miners will be purely transactional fees.

### Problems

In terms of security - there is no other consensus model out there more secure than PoW. However, many sees this as a problem with environmental impact as miners are "wasting energy" to solve a "redundant problem" and it is also a huge bottleneck for the network as it limits how many transactions can be processed as each block has a maximum size of 1mb. There are many heated debates on this topic which has sparked the creation of Proof of Stake - a more environmental friendly consensus model and also tweaks to the block size and difficulty to try and fit more transactions in per block as well as making finding blocks more quicker than the average of 10 minutes. More about this can be found in the advanced section.