## Addresses

An address in Bitcoin can be thought of as a wallet, or account which belongs to a certain user. There are two parts to an account, the public key (also known as a public address) and the private key. The public key can be thought of as you would an address - it lets others know where you are located and this is the destination for them to send Bitcoins to you. The private key on the other hand is like the key to the house, **only you should know this and you should never share this to anyone**. Letting the public know what your address (public key) is, is perfectly safe but only you should have the key to the house (private key) otherwise anyone can just walk into your house and steal everything from you.

To emphasis and reiterate:

<dl>
  <dt><strong>Public Key</strong></dt>
  <dd>A location for Bitcoins to be stored, the address others need to know in order for them to send Bitcoins for you to receive</dd>
  <dt><strong>Private Key</strong></dt>
  <dd>The key required to access the Bitcoins in the public key - allows the person to transact Bitcoins out of the address. Proof of Ownership of the public key</dd>
</dl>

The keys are generated using a technique known as [asymmetric key encryption](https://en.wikipedia.org/wiki/Public-key_cryptography). What they essentially allow us to do is to encrypt a message with one key, and decrypt it back to the original message with the other. A message can be pretty much anything, and not limited to text as anything digital is essentially just 1's and 0's anyway and can be easily converted into a "message". Or rather, a message can represent anything you want, be it a simple text message, an mp3 file or even a transaction object.

### Digital Signatures

With the power to encrypt and decrypt a message using two different keys, what this now allows us to do is to digitally sign a message and provide a quick way for others to verify authenticity of the message. This is best explained with an example:

    { value: 3, to: bitcoinAddress2, from: myPublicKeyAddress }

I have a transaction object that I want everyone to know that I have intended to do. The steps to digitally signing this are:

1. Use the private key to encrypt the transaction object:

          { value: 3, to: bitcoinAddress2, from: myPublicKeyAddress }
                                       ⇓
                         ||========================||
                         ||Encrypt with private key||
                         ||========================||
                                       ⇓
                    "xk76shdf7s8fdiSG68sis8at8a9t8yiut9G5a"

2. Publish the transaction along with the encrypted result - the encrypted result is known as the digital signature:

        { 
            transaction: { value: 3, to: bitcoinAddress2, from: myPublicKeyAddress },
            signature: "xk76shdf7s8fdiSG68sis8at8a9t8yiut9G5a"
        }
        

3. Now anyone who looks at my transaction can take that signature and try to decrypt it with the public key `myPublicKeyAddress` and if the results match the transaction object, the person can confirm that I must have knowledge of the private key that is paired with the public key `myPublicKeyAddress` and deduce that the transaction is legitimate:

                    "xk76shdf7s8fdiSG68sis8at8a9t8yiut9G5a"
                                       ⇓
              ||==============================================||
              || Decrypt with public key 'myPublicKeyAddress' ||
              ||==============================================||
                                       ⇓
          { value: 3, to: bitcoinAddress2, from: myPublicKeyAddress }
        
    *The decrypted transaction and the original transaction match, therefore the transaction is legitimate*

No one else is capable of transacting Bitcoins out of a public address without the private key because it would be impossible for them to construct such a digital signature that would decrypt back to the intended transaction. As in the example, there would have been no way to work out the signature `xk76shdf7s8fdiSG68sis8at8a9t8yiut9G5a` which is needed to create a transaction to move 3 BTC out of the address. This is how Bitcoin solves the ownership problem, when creating a transaction, proof of ownership is derived by the digital signature.