## Blocks

Blocks can be simply thought of as a list of transactions with some useful information stored in the header and each block can fit at most 1mb of data. There are actually 5 fields in a block structure but knowing that there is a list of transactions as well as a header is all you really need to understand. Blocks are what miners try to mine - miners are actually nodes that create valid blocks by solving a difficult problem. Blocks are roughly created every 10 minutes (2016 blocks every two weeks) and is regulated with the 'difficulty' to mine. So as explained, the body of the block basically just contains a list of transactions - the interesting part to look at is the block header.

### Block Header

The block header consists of 6 different parts:

Field | Purpose | Updated when... | Size (Bytes)
---|---|---|---
Version|Block version number|You upgrade the software and it specifies a new version|4
hashPrevBlock|256-bit hash of the previous block header|A new block comes in|32
hashMerkleRoot|256-bit hash based on all of the transactions in the block|A transaction is accepted|32
Time|Current block timestamp as seconds since 1970-01-01T00:00 UTC|Every few seconds|4
Bits|Current target in compact format|The difficulty is adjusted|4
Nonce|32-bit number (starts at 0)|A hash is tried (increments)|4

*Information courtesy of https://en.bitcoin.it/wiki/Block_hashing_algorithm*

#### Time

Each block header contains the time - this is what allows the network to keep a history of all transactions in chronological order.

#### hashMerkleRoot

The hashMerkleRoot is a single hash which is the 'parent' hash of all transactions. Without delving into too much details, this merkle root is generated from the hashes of every transaction in the block, which means that if any transactions are removed, added or altered, the merkle root would also change. It can be thought of as a 'fingerprint' for all transactions in the current block. See the advanced section on merkle root for more information.

#### hashPrevBlock

This is what ties the entire blockchain together and keeps everything linked. Since this block contains the hash of the previous block header, it means that if someone was to try to change history and alter previous blocks, then the hash of the previous block header would no longer match with what is stated in its successors block header. That means to make a change in a block, you would also have to update every single block after that as all their hashes are invalidated as they are dependant on each other. Since the merkle root of all transactions is included in the headers, it means any transactional changes will affect the merkle root, which in turn will affect the block headers hash which means the previous statement applies - all future blocks are affected and become invalid.

#### Nonce

The nonce is the solution to a very difficult problem which is an elegant solution that stops people from creating valid blocks at will. To understand this, we need to look at the next section about mining.