## Transactions

With the help of digital signatures, it allows us to solve the problem of ownership digitally. This alone however, is not enough to create a secure system to process transactions. We need to also solve what is known as the "Double Spending" problem, which is a potential flaw in a digital cash system where a user can spend the same Bitcoin more than once.

Consider the following situation:

1. Person A has 10 BTC.
2. Person A creates a transaction to send 10 BTC to person B.
3. Person A creates a transaction to send 10 BTC to person C.

In the scenario above, since person A owns 10 BTC, he can digitally sign a transaction to send 10 BTC to anyone. The problem is that person A can then sign another transaction to send the same 10 BTC to another person. There is no way to track what has been spent in the system therefore it is impossible to keep an accurate balance of all addresses. We don't know which transaction has happened - as long as you were a owner of a BTC at some point, then you can create a digital signature for a transaction and spend the same BTC as many times as you want. What we need is a way to track whether the BTC has already been spent or not. In the real world with digital cash, this is solved by tracking the balance of all accounts in a ledger held by banks. We need a similar system, that is decentralized, to solve this double-spending problem and this is what the blockchain is all about.

What Satoshi Nakamoto gave us, is Proof-of-Work, which gave birth to the blockchain. The blockchain is just as it sounds, a chain of blocks, and what this provides us with is a timestamped ledger - essentially a giant spreadsheet which keeps a history of everything that has happened and when it happened. This ledger can only be appended to, and cannot be altered. This allows us to timestamp and store every transaction made and therefore allow us to easily keep track of the balances of addresses by looking up the full history of all transactions made to and from the address from the blockchain.

With the same scenario again, when the second transaction is made to send 10 BTC to person C, the Bitcoin network is able to check the balance of person A's address by querying the blockchain and see that he had already made a transaction that spent 10 BTC and reject the second transaction as he has 0 BTC left in his address.