# Advanced Topics

This section is for people who wish for further learning about Bitcoin where the basic understanding of the blockchain is not enough to satisfy the individual. Here I shall be going over more intricate parts of Bitcoin in detail, discussions about different consensus models and answer some more advanced questions about design choices on Bitcoin.