# About Me

My name is Wai Get Law and I have always been fascinated with mathematics since High School and I have my maths teacher Mr. Nolan to thank for that. I also discovered an interest for programming during High School too but did not pursue into coding until I randomly stumbled upon it again in 2016 as a new career path and I am now an apprentice software developer for CGI.

I discovered Bitcoin around early 2017 but did not delve into it till late 2017 and ever since have been captivated by the ideaology of Bitcoin. I definitely started out more of an alt-coin enthusiast but I am now pretty much a Bitcoin Maximalist. The more I learned, the more Bitcoin just seemed to be the answer to everything.

### Contact

You can send me an email at: waigetlaw@gmail.com

### Other Projects

My website can be found here: https://waigetlaw.gitlab.io/  
My GitLab Profile: https://gitlab.com/waigetlaw

### Donations

If you like my work and would like to support me, I would be grateful :)

[Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=596MVU35ATDCS&source=url)
