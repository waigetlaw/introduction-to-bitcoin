# What Is Bitcoin?

Whenever someone asked me to tell them about Bitcoin and why it is so great, I used to make the mistake of trying to explain 'how' the code achieves security, transparency, decentralization, etc. but I learned over time that this was a bad approach. Not everyone is up to date with all the new technology and knows all the jargon thrown around in the land of computing but the fact is, the average person does not need to know 'how' Bitcoin achieves these characteristics, but rather 'why' they are important. To keep it simple, Bitcoin can be thought of as a digital store of value, you can think of it as digital gold, and its advantages can be summed up with five bullet points:

* Decentralized
* Transparent
* Censorship Resistant
* Unconfiscatable
* Secure

I will eventually go into more detail on 'how' Bitcoin achieves these traits but as an introduction, I want to explain why these traits are important so that even if you do not understand the technology behind it, you can still grasp at the primary goal of Bitcoin and see its potential and reason of existence.