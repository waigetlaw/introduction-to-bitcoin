## Comparisons

So now that I have briefly explained the properties of Bitcoin, we can have a comparison on what other options we have to evaluate Bitcoin's position and future possibilities. I do want to very quickly explain what store of value is with this quote straight out of [wikipedia](https://en.wikipedia.org/wiki/Store_of_value):

> A store of value is the function of an asset that can be saved, retrieved and exchanged at a later time, and be predictably useful when retrieved. More generally, a store of value is anything that retains purchasing power into the future.

In other words, store of value has long-term value so that we can, if so desired, accumulate our purchasing power over time and spend it when we want to with minimal loss in purchasing power had we spent it as acquired. 

### Comparison with Fiat

Currency is one example of store of value but it is not always a great choice. With currency run by banks and governments that can freely print out money which can cause overinflation, silently stealing your purchasing power away from you. Bitcoin on the otherhand has a strictly regulated protocol for creating new Bitcoins through the mining process and the circulation at a given date is accurately predictable with a finite amount set to be 21million. It is also arguably harder to trace transactions with fiat than Bitcoin when people use the argument of Bitcoin being used in the black market. There is no real system put in place to keep track of fiat money being passed from person-to-person in a shady exchange whereas with Bitcoin, the transaction history will always be public on the blockchain. There is also a lot of counterfeit money in circulation, according to the United States Department of Treasury, an estimated $70 million in counterfeit bills are in circulation which causes a lot of problems in business when dealing with physical money. Bitcoin being digital with cryptographic technology bypasses this problem as it is impossible to create a legit Bitcoin out of thin-air. Furthermore as discussed in the earlier chapters, fiat is heavily controlled by the banking system which means your money is as safe as the banks are trustworthy - with Bitcoin, you don't need to trust any third-party.

### Comparison with Gold

Gold has historically been used as a store of value even to this present day and has proved itself to be a safe choice with the market cap of around $7trillion being self evident. Gold is good as a store of value because it has the qualities needed to be used as money. 

* It is naturally scarce, and rarity is important to hold its store of value
* It is fungible in the sense that all gold is more or less identical
* It is divisible, as transactions can come in any size
* It is widely-accepted, if people don't recognise it as a store of value and refuses to trade then it's purchasing power is essentially zero
* Unconfiscatable - gold is generally considered unconfiscatable until the invention of metal detectors

Without me having to say, I'm sure all of you agree that gold has been and still is a good store of value. However, when comparing with Bitcoin, there isn't any real advantages that gold has over it except for being much more widely-accepted at present. Bitcoin, despite being man-made is also scarce since there is a finite amount. It is more fungible than gold as every Bitcoin is just some 1's and 0's on the network - the only difference being the transaction history that the Bitcoin has had. With gold, they are never quite identical. One gold bar might be 99% pure gold, with another being 99.99% pure gold and therefore should be evaluated with a difference in value. Bitcoin being digital is easily more divisible than gold - currently the smallest division (known as a Satoshi) is one hundred millionth of a bitcoin, ฿0.00000001, but it can be adjusted accordingly if even smaller divisions are needed.

>Bitcoins can be divided up to 8 decimal places (0.000 000 01) and potentially even smaller units if that is ever required in the future as the average transaction size decreases.
>
>From [bitcoin.org](https://bitcoin.org/en/faq#wont-the-finite-amount-of-bitcoins-be-a-limitation)

In terms of unconfiscatable, Bitcoin also has the advantage here as the chances of someone accurately guessing your private key is astronomically lower than finding the location of the gold - especially with the invention of metal detectors.

Finally, whilst during the 2017 bull market run up for Bitcoin it is true that we saw ridiculous transaction fees which shows Bitcoin is just not ready for such a huge amount of transaction volume. In the future Bitcoin intends to solve this problem by introducing the Lightning Network which will be able to handle most transactions off-chain whilst still maintaining the security that the blockchain offers. In contrast, for the average person, transacting in gold may have never seemed like a problem and seem very portable but when trying to transact a large sum of money in gold, it is actually very difficult to move such volume of gold from one location to another, especially overseas. When shipping large amounts of gold, the cost to phyiscally move the gold costs an uncomparable amount more than the cost of Bitcoin transactions as nothing physically needs to be moved on the network. Storage is also another huge problem from gold when dealing with large amounts and in contrast, most people are safe enough with just a simple ledger or paper wallet provided you follow good security precautions.