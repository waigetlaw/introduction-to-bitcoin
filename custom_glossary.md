# Glossary

Here you can find some common terms and their definitions.

### Bitcoin

Decentralized Digital currency invented by Satoshi Nakamoto based on cryptography.

### Blockchain

Name given to the idea of chaining blocks which contain the hash of the previous block creating an immutable ledger.

### Blocks

Contains transactions and other information such as the hash of the previous block, merkle root of all the transactions, the nonce, etc.

### Block Time

The average time between each block, for Bitcoin it is set to 2016 blocks every 2 weeks, which is 10 minutes per block.

### Coinbase

Not to be confused with the exchange called Coinbase, this is the term given for the transaction on a block which contains the block reward and goes to the miner.

### Difficulty

How hard it is to solve for a block, the number of leading zeroes needed to satisfy the Proof of Work.

### Encryption

A two-way function. A message can be encrypted by some algorithm along with a secret, and the encrypted message can also be decrypted back to the original message by use of some known algorithm with the same secret used during the encryption (or by public-private key pair).

### Hash

A hash is the result of a one-way function. It is computationally cheap to verify a hash and virtually impossible to reverse the hash back to the original message.

### Mining

The act of creating blocks, mostly with transactions, to add to the blockchain and is rewarded with Bitcoin.

### Nonce

The name given to the solution to obtaining a hash with enough leading zeroes to satisfy the Proof of Work.

### Private Key

One of the key pair in the generation of an address which gives proof of ownership, used to digitally sign transactions.

### Proof of Work

The consensus model invented by Satoshi Nakamoto which involves finding a hash with X amount of leading zeroes.

### Public Key

One of the key pair in the generation of an address, used for others to send Bitcoin to the address.

### Satoshi (unit)

A one hundred millionth of a single bitcoin (0.00000001 BTC) named after the original creator of the paper Satoshi Nakamoto.
